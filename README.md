# HeightmapGenerator

A c++ heightmap generator

## Getting Started

* Note this is handled with static memory for performance where the maximum length is given by the maplen.h file.

See the exec.cpp file for execution of various problems.

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.