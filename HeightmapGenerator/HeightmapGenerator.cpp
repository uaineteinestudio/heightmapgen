#include "heightmapGenerator.h"

heightmapGenerator::heightmapGenerator(int w, int h, int smthFac)
{
	width = w;
	height = h;
	smoothFac = smthFac;
}

heightmapGenerator::~heightmapGenerator()
{
}

int ** heightmapGenerator::genNewMap()
{
	int** map = makeRandomHeightMap();
	smooth(map);
	return map;
}

int ** heightmapGenerator::makeRandomHeightMap()
{
	/* initialize random seed: */
	srand(time(NULL));

	int** h = 0;							//give 0 value for now
	h = new int*[MaxLen];					//x vals
	for (int x = 0; x < MaxLen; x++)
	{
		h[x] = new int[MaxLen];				//y vals
		for (int y = 0; y < MaxLen; y++)
		{
			h[x][y] = rand() % MaxHeight;	//0 to max height
		}
	}
	return h;
}

bool heightmapGenerator::inBounds(int xi, int yi)
{
	return boundchecker::inBounds(xi, yi, width, height);
}

bool heightmapGenerator::inMaxBounds(int xi, int yi)
{
	return boundchecker::inBounds(xi, yi, MaxLen, MaxLen);
}

void heightmapGenerator::smoothOnce(int ** map)
{
	/* initialize random seed: */
	srand(time(NULL));
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			int no = 0;
			int avg = getAvg(map, x, y, no);
			//now to add random var
			int var = (rand() % 3) - 1;	//variation between -1 and 1;
			map[x][y] = avg + var;
		}
	}
}

void heightmapGenerator::smooth(int** map)
{
	for (int i = 0; i < smoothFac; i++)
	{
		smoothOnce(map);
	}
}

int heightmapGenerator::getAvg(int** map, int xi, int yi, int &no)//return number
{
	int col[5] = {-1, 0, 0, 0, 1};
	int row[5] = { 0, -1, 0, 1, 0 };

	no = 0;
	int sum = 0;
	//iterate over the coords for the neighbours
	for (int i = 0; i < 5; i++)
	{
		int x = xi + col[i];
		int y = yi + row[i];
		if (inMaxBounds(x, y))
		{
			no +=1;
			sum += map[x][y];//add value
		}
	}
	if (no > 0)
		return sum / no;//get sum over number as average;
	else return map[xi][yi];
}
