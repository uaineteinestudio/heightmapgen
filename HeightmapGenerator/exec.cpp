#include <iostream>
#include "heightmapGenerator.h"

using namespace std;

void print_matrix(int** matrix, int width, int height)
{
	cout << endl;
	for (int i = 0; i < width; i++)
	{
		for (int j = 0; j < height; j++)
		{
			cout << matrix[i][j] << " ";
		}
		cout << endl;
	}
}

void heightmapTest()
{
	int width = 20;
	int height = 20;
	heightmapGenerator hGen = heightmapGenerator(width, height, 3);
	int** map = hGen.genNewMap();
	print_matrix(map, width, height);
}

int main()
{
	//std::cout << "Hello World!\n";
	heightmapTest();
}