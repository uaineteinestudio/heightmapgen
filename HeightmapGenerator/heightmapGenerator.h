#pragma once

#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include "boundchecker.h"
#include "maplen.h"

class heightmapGenerator
{
public:
	heightmapGenerator(int w, int h, int smthFac);
	~heightmapGenerator();
	int** genNewMap();
	int** makeRandomHeightMap();
	bool inBounds(int xi, int yi);
	bool inMaxBounds(int xi, int yi);
protected:
	void smoothOnce(int** map);
	void smooth(int** map);
	int getAvg(int** map, int xi, int yi, int &no);//return number
private:
	int width;
	int height;
	int smoothFac;
	static const int MaxHeight = 512;//changeable
};

