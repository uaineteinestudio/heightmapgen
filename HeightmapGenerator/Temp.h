#pragma once

#include "maplen.h"

using namespace std;

class tempCalc
{
public:
	tempCalc(int dimlen, int smthcont);//output should be between 0 and 1
	~tempCalc();
	float** calctemp(int** type, int** height);
	static float tmpvalue(int type, int height);//to get the temp value for a tile type. This is a changeable method
private:
	int len;
	int smoothfac;
	float getvecval(float** cur, int x, int y, int & num);
	float getvecval(float cur[MaxLen][MaxLen], int x, int y, int & num);
	void Normalise(float** cur);
	float largest(float** cur);
	void smoothit(float** cur);
};