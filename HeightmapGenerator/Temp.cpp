#include "Temp.h"
#include "boundchecker.h"

tempCalc::tempCalc(int dimlen, int smthcont)
{
	len = dimlen;
	smoothfac = smthcont;
}

tempCalc::~tempCalc()
{
}

float ** tempCalc::calctemp(int** type, int** height)
{
	float** map = new float*[MaxLen];
	for (int x = 0; x < MaxLen; x++)
	{
		map[x] = new float[MaxLen];
		for (int y = 0; y < MaxLen; y++)
		{
			int t = type[x][y];
			int h = height[x][y];
			map[x][y] = tmpvalue(t, h);
		}
	}
	//now smooth
	for (int i = 0; i < smoothfac; i++)
	{
		smoothit(map);
	}
	Normalise(map);
}

float tempCalc::tmpvalue(int type, int height)
{
	float val = 0;
	switch (type)
	{
	case 0:
		val = 1;
		break;
	case 1:
		val = 0.3;
		break;
	case 4://I hope this is water
		val = 0.0001;
		break;
	case 10:
		val = 0.5;
		break;
	default:
		val = 1;
		break;
	}
	val -= height * 0.001;
	return val;
}

void tempCalc::smoothit(float** cur)
{
	int col[8] = { -1, -1, -1, 0, 0, 1, 1, 1 };
	int row[8] = { 0, -1, 1, -1, 1, -1, 0, 1 };

	//make old list
	float old[MaxLen][MaxLen];
	for (int x = 0; x < MaxLen; x++)
	{
		for (int y = 0; y < MaxLen; y++)
		{
			old[x][y] = cur[x][y];
		}
	}

	for (int x = 0; x < len; x++)
	{
		for (int y = 0; y < len; y++)
		{
			float smthed = cur[x][y];
			int count = 1;
			for (int k = 0; k < 8; k++)
			{
				smthed += getvecval(old, x + col[k], y + row[k], count);
			}
			smthed = smthed / count;
			//now overwrite that value?
			cur[x][y] = smthed;
		}
	}
}

float tempCalc::getvecval(float** cur, int x, int y, int & num)
{
	float out = 0;
	if (boundchecker::inBounds(x, y, MaxLen, MaxLen))
	{
		out = cur[x][y];
		num += 1;
	}
	return out;
}

float tempCalc::getvecval(float cur[MaxLen][MaxLen], int x, int y, int & num)
{
	float out = 0;
	if (boundchecker::inBounds(x, y, MaxLen, MaxLen))
	{
		out = cur[x][y];
		num += 1;
	}
	return out;
}

void tempCalc::Normalise(float** cur)
{
	float greatest = largest(cur);
	for (int x = 0; x < len; x++)
	{
		for (int y = 0; y < len; y++)
		{
			cur[x][y] = cur[x][y]/greatest;//normalise for the set to be between 0 and 1.
		}
	}
}

float tempCalc::largest(float** cur)
{
	float greatest = 0;
	for (int x = 0; x < len; x++)
	{
		for (int y = 0; y < len; y++)
		{
			if (cur[x][y] > greatest)
				greatest = cur[x][y];//overwrite it
		}
	}
	return greatest;
}
